# Info for interviewee

Let's assume situation like this:

Your manager has met you on a corridor and said following:
``` 
    I need you to take over a small application called exchange-rates. 
    We were supposed to release version 2 of this service today but this Sonar tool send me an email saying something about bad code quality. 
    The guy that coded that is no longer with the company but he said he tested this manually and it's fine. 
    Check this project if it looks fine and fix whatever you need. We need to release this today so you have an hour or so.     
```

So, your task is to improve code quality and ensure that requirements for version 2 are meet.

Below is all documentation there is, good luck.

---


# Exchange-Rates

## Project requirements

### Version 1

Endpoint should be created that allows our clients to check exchange rates published by European Central Bank (should use `https://exchangeratesapi.io/` as a data source). We will add 2% spread on top of official exchange rates.
Endpoint should expect GET request with 1 parameter:
- `to` - target currency of exchange rate
It provides exchange rate of EUR to provided currency - foe example when `to` is `GBP` it will provide exchange rate `EUR->GBP`.
Response should contain exchange rate and names of currencies used to calculate exchange rate.

### Version 2

Due to clients demands we need to provide a new endpoint. Following requirements should be meet.
1. New endpoint should expect GET request with 1 parameter:
   - `targetCurrency` - same as `to` in the previous version of endpoint - clients just want a better name
2. There are cases where clients use country code as currency. We should allow that for 3 most used currencies:
   - for `US` or `USA` we should use `USD` as currency
   - for `GB` or `UK` we should use `GBP` as currency
   - for `EU` or `EURO` we should use `EUR` as currency

    So, if clients requests exchange rate for `US` we should return exchange rate for `USD`. 
3. As in previous endpoint exchange rates should be taken from European Central Bank and 2% spread should be added.
4. Also, clients requested additional information to be given in response. There should be name and some base information added for base and target currency. We can take this from Wikipedia - this will be ok most of the time. 

---

## Dev project info

This is a SpringBoot application packaged as JAR. To run this application you will need Maven and Java 11.
Main class: com/refactor/exchangerates/ExchangeRatesApplication.java 

There are 2 endpoints:
- `/v1/exchanges` - this endpoint returns exchange rate between 2 currencies using `api.exchangeratesapi.io` as a source of data
- `/v2/exchanges` - this endpoint returns exchange rate between 2 currencies using `api.exchangeratesapi.io` as a source of data and provides extra information about each currency using wikipedia (`en.wikipedia.org`) as a source

To test how they work you can open swagger-UI using URL `http://localhost:8080/swagger-ui/index.html`