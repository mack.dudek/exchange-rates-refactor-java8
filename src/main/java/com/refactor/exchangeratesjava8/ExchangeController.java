package com.refactor.exchangeratesjava8;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Map;

@RestController
public class ExchangeController {

    @GetMapping("/v1/exchanges")
    public ExchangeResponse getExchangesV1(@RequestParam String to) {
        ExchangeResponse response = new ExchangeResponse();
        response.setFrom(to);
        response.setTo(to);
        double exchangeRate = getExchangeRate(to);
        if(exchangeRate>1) { exchangeRate = exchangeRate + exchangeRate * 2.0 / 100.0; }
        else if((exchangeRate<1)) { exchangeRate = exchangeRate - exchangeRate * 2.0 / 100.0; }
        else { exchangeRate = 1.02; }
        response.setExchangeRate(exchangeRate);
        return response;
    }

    /**
     *
     * To get EUR to GBP exchange rate URL should look like this
     * http://api.exchangeratesapi.io/v1/latest?symbols=GBP&access_key=b6302a94d6eb781d1f8b3b0080432ccb
     * exchangeratesapi.io response looks like this:
     * {"success":true,"timestamp":1636529763,"base":"EUR","date":"2021-11-10","rates":{"GBP":0.853594}}
     *
     * @param from from
     * @param to to
     * @return exchange rate
     */
    public double getExchangeRate(String to) {
        ExchangeRatesApiResponse response =
                WebClient.create()
                .get()
                .uri(builder -> builder.scheme("http")
                .host("api.exchangeratesapi.io")
                .path("/v1/latest")
                .queryParam("symbols", to)
                .queryParam("access_key", "b6302a94d6eb781d1f8b3b0080432ccb")
                .build())
                .retrieve()
                .bodyToMono(ExchangeRatesApiResponse.class)
                .block();
        System.out.println(response);
        return response.rates.get(to);
    }

    public static class ExchangeRatesApiResponse {
        Map<String, Double> rates;
        String base;
        String date;

        public Map<String, Double> getRates() {
            return rates;
        }

        public void setRates(Map<String, Double> rates) {
            this.rates = rates;
        }

        public String getBase() {
            return base;
        }

        public void setBase(String base) {
            this.base = base;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        @Override
        public String toString() {
            return "ExchangeRatesApiResponse{" +
                    "rates=" + rates +
                    ", base='" + base + '\'' +
                    ", date='" + date + '\'' +
                    '}';
        }
    }

    public static class ExchangeResponse {
        String from;
        String to;
        double exchangeRate;

        public String getFrom() {
            return from;
        }

        public void setFrom(String from) {
            this.from = from;
        }

        public String getTo() {
            return to;
        }

        public void setTo(String to) {
            this.to = to;
        }

        public double getExchangeRate() {
            return exchangeRate;
        }

        public void setExchangeRate(double exchangeRate) {
            this.exchangeRate = exchangeRate;
        }
    }
}
