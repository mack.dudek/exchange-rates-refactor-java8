package com.refactor.exchangeratesjava8.v2;


import com.refactor.exchangeratesjava8.ExchangeController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class ExchangeControllerV2 {

    @Autowired
    public ExchangeController exchangeController;

    @Autowired
    public WikiClient wikiClient;


    @GetMapping("/v2/exchanges")
    public ExchangeResponseV2 getExchangesV2(@RequestParam String targetCurrency) {
        ExchangeResponseV2 exchangeResponseV2 = new ExchangeResponseV2();
        exchangeResponseV2.setExchangeRate(exchangeController.getExchangesV1(targetCurrency).getExchangeRate());
        exchangeResponseV2.setBaseCurrency("EUR");
        if(targetCurrency == "US") targetCurrency = "USD";
        if(targetCurrency == "USA") targetCurrency = "USD";
        if(targetCurrency == "GB") targetCurrency = "GBP";
        if(targetCurrency == "UK") targetCurrency = "GBP";
        if(targetCurrency == "EU") targetCurrency = "EUR";
        if(targetCurrency == "EURO") targetCurrency = "EUR";
        exchangeResponseV2.setTargetCurrency(targetCurrency);
        exchangeResponseV2.setBaseCurrencyDescription(convert(wikiClient.getCurrencyInfo("EUR")));
        exchangeResponseV2.setTargetCurrencyDescription(convert(wikiClient.getCurrencyInfo(targetCurrency)));
        return exchangeResponseV2;
    }

    private ExchangeResponseV2.CurrencyDescription convert(WikiClient.WikiDescription wikiDescription) {
        return new ExchangeResponseV2.CurrencyDescription(wikiDescription.title, wikiDescription.snippet);
    }

    public static class ExchangeResponseV2 {
        String baseCurrency;
        String targetCurrency;
        CurrencyDescription baseCurrencyDescription;
        CurrencyDescription targetCurrencyDescription;
        double exchangeRate;

        public String getBaseCurrency() {
            return baseCurrency;
        }

        public void setBaseCurrency(String baseCurrency) {
            this.baseCurrency = baseCurrency;
        }

        public String getTargetCurrency() {
            return targetCurrency;
        }

        public void setTargetCurrency(String targetCurrency) {
            this.targetCurrency = targetCurrency;
        }

        public CurrencyDescription getBaseCurrencyDescription() {
            return baseCurrencyDescription;
        }

        public void setBaseCurrencyDescription(CurrencyDescription baseCurrencyDescription) {
            this.baseCurrencyDescription = baseCurrencyDescription;
        }

        public CurrencyDescription getTargetCurrencyDescription() {
            return targetCurrencyDescription;
        }

        public void setTargetCurrencyDescription(CurrencyDescription targetCurrencyDescription) {
            this.targetCurrencyDescription = targetCurrencyDescription;
        }

        public double getExchangeRate() {
            return exchangeRate;
        }

        public void setExchangeRate(double exchangeRate) {
            this.exchangeRate = exchangeRate;
        }

        @Override
        public String toString() {
            return "ExchangeResponseV2{" +
                    "baseCurrency='" + baseCurrency + '\'' +
                    ", targetCurrency='" + targetCurrency + '\'' +
                    ", baseCurrencyDescription=" + baseCurrencyDescription +
                    ", targetCurrencyDescription=" + targetCurrencyDescription +
                    ", exchangeRate=" + exchangeRate +
                    '}';
        }

        public static class CurrencyDescription {
            String name;
            String description;

            public CurrencyDescription(String name, String description) {
                this.name = name;
                this.description = description;
            }

            public String getName() {
                return name;
            }

            public void setName(String name) {
                this.name = name;
            }

            public String getDescription() {
                return description;
            }

            public void setDescription(String description) {
                this.description = description;
            }

            @Override
            public String toString() {
                return "CurrencyDescription{" +
                        "name='" + name + '\'' +
                        ", description='" + description + '\'' +
                        '}';
            }
        }
    }
}
