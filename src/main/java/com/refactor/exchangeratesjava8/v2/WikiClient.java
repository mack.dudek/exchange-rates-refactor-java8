package com.refactor.exchangeratesjava8.v2;

import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@Component
public class WikiClient {

    /**
     *
     * Will call wiki like this:
     * https://en.wikipedia.org/w/api.php?action=query&list=search&srlimit=1&srsearch=USD&utf8=&format=json
     * Change "srsearch" to search for other currencies
     * Response is like this:
     * {
     *     "batchcomplete": "",
     *     "continue": {
     *         "sroffset": 1,
     *         "continue": "-||"
     *     },
     *     "query": {
     *         "searchinfo": {
     *             "totalhits": 18234
     *         },
     *         "search": [{
     *                 "ns": 0,
     *                 "title": "United States dollar",
     *                 "pageid": 18717338,
     *                 "size": 93694,
     *                 "wordcount": 9167,
     *                 "snippet": "The United States dollar (symbol: $; code: <span class=\"searchmatch\">USD</span>; also abbreviated US$ to distinguish it from other dollar-denominated currencies; referred to as the dollar",
     *                 "timestamp": "2020-09-12T03:17:05Z"
     *             }
     *         ]
     *     }
     * }
     *
     * @param currencyName name of currency
     * @return information from wikipedia
     */
    public WikiDescription getCurrencyInfo(String currencyName) {
        WikiResponse response =
                WebClient.create()
                        .get()
                        .uri(builder -> builder.scheme("https")
                                .host("en.wikipedia.org")
                                .path("w/api.php")
                                .queryParam("action", "query")
                                .queryParam("list", "search")
                                .queryParam("srlimit", 1)
                                .queryParam("format", "json")
                                .queryParam("srsearch", currencyName)
                                .build())

                        .retrieve()
                        .bodyToMono(WikiResponse.class)
                        .block();
        System.out.println(response);
        WikiResponse.Query.SearchResults searchResults = response.query.search.get(0);
        return new WikiDescription(searchResults.title, searchResults.snippet);
    }

    public static class WikiResponse {
        Query query;

        public Query getQuery() {
            return query;
        }

        public void setQuery(Query query) {
            this.query = query;
        }

        @Override
        public String toString() {
            return "WikiResponse{" +
                    "query=" + query +
                    '}';
        }

        public static class Query {
            List<SearchResults> search;

            public List<SearchResults> getSearch() {
                return search;
            }

            public void setSearch(List<SearchResults> search) {
                this.search = search;
            }

            @Override
            public String toString() {
                return "Query{" +
                        "search=" + search +
                        '}';
            }

            public static class SearchResults {
               String title;
               String snippet;

                public String getTitle() {
                    return title;
                }

                public void setTitle(String title) {
                    this.title = title;
                }

                public String getSnippet() {
                    return snippet;
                }

                public void setSnippet(String snippet) {
                    this.snippet = snippet;
                }

                @Override
                public String toString() {
                    return "SearchResults{" +
                            "title='" + title + '\'' +
                            ", snippet='" + snippet + '\'' +
                            '}';
                }
            }
        }
    }

    public static class WikiDescription {
        public String title;
        public String snippet;

        public WikiDescription(String title, String snippet) {
            this.title = title;
            this.snippet = snippet;
        }
    }
}
